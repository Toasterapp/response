export interface ResponseFormat {
    rescode: number;
    errorcode: number;
    log: string;
    err: string;
}
export interface ErrorcodeFormat {
    errcode: number;
    errmsg: string;
}
export declare class Response {
    errlist: Array<ErrorcodeFormat>;
    constructor(errmsglist: {
        errcode: number;
        errmsg: string;
    }[]);
    make_responce(errorcode: number | null, log?: string): ResponseFormat;
}
export declare class ErrorcodeFormatFileReader {
    static readSync(fullpath: string): ErrorcodeFormat[];
}
