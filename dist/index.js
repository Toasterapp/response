"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
//import {ResponseFormat} from "tst_response";
var fs = __importStar(require("fs"));
var Response = /** @class */ (function () {
    function Response(errmsglist) {
        var _this = this;
        this.errlist = new Array();
        errmsglist.forEach(function (element) {
            _this.errlist.push({ errcode: element.errcode, errmsg: element.errmsg });
        });
    }
    Response.prototype.make_responce = function (errorcode, log) {
        if (log === void 0) { log = ""; }
        var res = { rescode: 0, errorcode: 0, log: "", err: "" };
        console.log(errorcode);
        if (errorcode === null) {
            res.rescode = 1;
            res.log = log;
            return res;
        }
        else {
            var matcherr = this.errlist.filter(function (element) { return element.errcode == errorcode; });
            if (matcherr.length > 0) {
                res.errorcode = errorcode;
                res.rescode = -1;
                res.err = matcherr[0].errmsg;
            }
            else {
                res.errorcode = errorcode;
                res.rescode = -1;
                res.err = "";
            }
            return res;
        }
    };
    return Response;
}());
exports.Response = Response;
var ErrorcodeFormatFileReader = /** @class */ (function () {
    function ErrorcodeFormatFileReader() {
    }
    ErrorcodeFormatFileReader.readSync = function (fullpath) {
        var object = fs.readFileSync(fullpath, 'utf8');
        var json = JSON.parse(object);
        return json;
    };
    return ErrorcodeFormatFileReader;
}());
exports.ErrorcodeFormatFileReader = ErrorcodeFormatFileReader;
