//import {ResponseFormat} from "tst_response";
import * as fs from "fs";
export interface ResponseFormat {
    rescode: number;
    errorcode: number;
    log: string;
    err: string;

}

export interface ErrorcodeFormat {
    errcode: number;
    errmsg: string;
}

export class Response {
    errlist: Array<ErrorcodeFormat>;
    constructor(errmsglist: { errcode: number; errmsg: string; }[])
    {
        this.errlist = new Array<ErrorcodeFormat>();
        errmsglist.forEach((element: { errcode: number; errmsg: string; }) => {
            this.errlist.push({errcode: element.errcode,errmsg: element.errmsg});
        });
    }
    make_responce(errorcode: number | null, log: string = ""): ResponseFormat {
        let res: ResponseFormat = { rescode: 0, errorcode: 0, log: "", err: "" };
        console.log(errorcode);
        if (errorcode === null) {
            res.rescode = 1;
            res.log = log;
            return res;
        }
        else {

            let matcherr: ErrorcodeFormat[] = 
            this.errlist.filter(element=>element.errcode == errorcode);
            if (matcherr.length > 0)
            {
                res.errorcode = errorcode;
                res.rescode = -1;
                res.err = matcherr[0].errmsg;
            }else{
                res.errorcode = errorcode;
                res.rescode = -1;
                res.err = "";
            }

        return res;


        }
    }

}
export class ErrorcodeFormatFileReader
{
    static readSync(fullpath: string):ErrorcodeFormat[]
    {
        var object = fs.readFileSync(fullpath, 'utf8');
        var json = JSON.parse(object) as ErrorcodeFormat[];
        return json;

    }
}